#!/bin/sh
set -e

# Create ssh private key file.
if [ ! -z "$SSH_PRIVATE_KEY" ]; then
  echo "Entrypoint: Creating private key file and setting GIT_SSH_COMMAND env variable."
  echo "${SSH_PRIVATE_KEY}" > /home/www-data/ssh_private_key
  chmod 600 /home/www-data/ssh_private_key
  export GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -i /home/www-data/ssh_private_key -o IdentitiesOnly=yes"
fi

# this will check if there are arguments.
if [ "$#" -eq 0 ]; then
  # No arguments, then keep on running.
  while true; do sleep 5; done;
fi

exec "$@"
