Setup multiarch build environment on your workstation.

```
docker run --rm --privileged docker/binfmt:820fdd95a9972a5308930a2bdfb8573dd4447ad3
cat /proc/sys/fs/binfmt_misc/qemu-aarch64
docker buildx create --name mybuilder
docker buildx use mybuilder
docker buildx inspect --bootstrap
```

Build multiarch image.
```
docker buildx build --platform linux/arm,linux/arm64,linux/amd64 --no-cache --push --tag [username]/[image-name]:[tag-name] .
```