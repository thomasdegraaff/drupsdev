function drupal_build() {

  # Building Drupal.
  echo "Building Drupal."

  cd /app

  # Check if existing drupal config exists.
  if [ -f "/app/composer.json" ]; then
    # Create Drupal codebase from existing config.
    echo "Installing Drupal codebase based on existing composer.json file."
    composer --no-interaction --working-dir="/app" install --ignore-platform-reqs
    check_result $?
  else
    # Create Drupal codebase.
    echo "Creating new drupal codebase."
    rm -rf /tmp/build
    composer create-project --no-install --no-interaction --ignore-platform-reqs "drupal/recommended-project:${drupal_release}" /tmp/build
    cp /tmp/build/composer.* /app/
    composer config --no-plugins allow-plugins.drupal/core-project-message true
    composer config --no-plugins allow-plugins.drupal/core-composer-scaffold true
    composer config --no-plugins allow-plugins.composer/installers true
    composer config process-timeout 1200;

    # Cleanup project message.
    echo "Cleaning up Drupal core project message."
    composer remove --no-install --ignore-platform-reqs drupal/core-project-message
    check_result $?

    # Get drush.
    echo "Get drush."
    composer require --no-install --ignore-platform-reqs "drush/drush:^${drush_version}"
    check_result $?

    # Add drups modules.
    #composer require --no-install --ignore-platform-reqs drupal/big_pipe_sessionless drupal/error_log drupal/purge:3.x-dev drupal/purge_purger_http drupal/queue_ui drupal/redis drupal/simple_sitemap:4.0.0 drupal/warmer
    #check_result $?

    # Add purge module deprecated php 8.1 patch.
    #composer require --no-install --ignore-platform-reqs cweagans/composer-patches
    #composer config --no-plugins allow-plugins.cweagans/composer-patches true
    #composer config extra.enable-patching true
    #composer config --json extra.patches.drupal/purge '{"Fix deprecated errors.": "https://www.drupal.org/files/issues/2022-02-24/3259320-10.patch"}'

    # Composer install.
    composer install --no-interaction --ignore-platform-reqs
    check_result $?
  fi

  # Add drups profile.
  #echo "Adding drups installation profile."
  #cp -r "/app/drupal/profiles/drups" "${drupal_root}/profiles/"

  #if [ "$LANDO" = 'ON' ]; then
    # Create opcode clear code, to be executed by visiting url.
    #echo "Create opcache_reset.php in the Drupal root for clearing the opcache."
    #echo '<?php opcache_reset(); ?>' > "${drupal_root}/opcache-reset.php"
  #fi

  # Copy drupal and lando settings files.
  echo "Create drupal settings"
  # Settings.php.
  cp "${site_dir}/default.settings.php" "${site_dir}/settings.php"
  cat "${drupal_settings_dir}/settings.append" >> "${site_dir}/settings.php"
  # Settings.local.php.
  cp "${drupal_root}/sites/example.settings.local.php" "${site_dir}/settings.local.php"
  # Copy settings to site dir.
  cp "${drupal_settings_dir}/settings.lando.php" "${site_dir}/settings.lando.php"
  cp "${drupal_settings_dir}/settings.cluster.php" "${site_dir}/settings.cluster.php"

  # Create config directory.
  mkdir -p "${drupal_config_dir}"
  cp "${drupal_settings_dir}/.htaccess" "${drupal_config_dir}/"

  # Ready.
  echo "Build finished."
  ok
}
