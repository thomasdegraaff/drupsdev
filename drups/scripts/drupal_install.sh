 function drupal_install() {

  # Check if composer.json exists.
  check_composer_json

  # Check if Drupal is already installed.
  cd "${drupal_root}"
  if /app/vendor/bin/drush status --field=bootstrap | grep -q Successful; then
    # Drupal is running.
    echo "Functional Drupal install found."

    # Set admin password.
    echo "Setting admin password to ${DRUPAL_ADMIN_PASSWORD}"
    /app/vendor/bin/drush user:password admin "${DRUPAL_ADMIN_PASSWORD}"

  else
    # Disable redis config.
    export REDIS_ENABLED="FALSE"
    # Create new site
    echo "No functional Drupal found, installing Drupal."
    cd "${site_dir}"
    #/app/vendor/bin/drush --yes site:install drups --site-name="Drups" --account-name=admin --account-pass="${DRUPAL_ADMIN_PASSWORD}"
    /app/vendor/bin/drush --yes site:install --site-name="Drups" --account-name=admin --account-pass="${DRUPAL_ADMIN_PASSWORD}"
    check_result $?
    echo -e "\nYou can login with the following credentials.\n\n  user: admin\n  password: ${DRUPAL_ADMIN_PASSWORD}.\n"
    # Enable redis config.
    export REDIS_ENABLED="TRUE"
  fi

  # Run database updates.
  echo "Running database updates."
  /app/vendor/bin/drush updatedb
  check_result $?

  # Rebuild cache.
  echo "Rebuilding caches."
  /app/vendor/bin/drush cache:rebuild
  check_result $?

  # Import configuration.
  echo "Import configuration."
  if find "${drupal_config_dir}" -type f -name '*.yml' | grep .; then
    /app/vendor/bin/drush -q config:import
    check_result $?
    # Rebuild cache.
    echo "Rebuilding caches."
    /app/vendor/bin/drush cache:rebuild
    check_result $?
  else
    echo 'No config found.'
    ok
  fi

  #if [ "$KUBERNETES" == "ON" ]; then
    # Correct files ownership.
    echo "Setting file permissions."
    # Set mode.
    chmod 440 "${site_dir}"/settings.*
    #chown www-data:www-data "${site_dir}"/settings.*
    # Create files directories and permissions.
    mkdir -p "${site_dir}/files"
    #chown -R www-data:www-data "${site_dir}/files"
    chmod 2775 "${site_dir}/files"
    # Set private files directory permissions.
    #chown -R www-data:www-data "/app/drupal/private_files"
    chmod 2775 "/app/drupal/private_files"
  #fi

  # Set varnish hostname for purge.
  #echo "Setting purge hostname."
  #/app/vendor/bin/drush -y config:set purge_purger_http.settings.58de7f60e3 hostname "${VARNISH_HOST}"
  #/app/vendor/bin/drush -y config:set purge_purger_http.settings.a07fe200d1 hostname "${VARNISH_HOST}"

  # Set Drupal cron key.
  echo "Setting Drupal cron key to '${DRUPAL_CRON_KEY}'."
  /app/vendor/bin/drush state:set system.cron_key "${DRUPAL_CRON_KEY}"

  # Set simple sitemap base url.
  #echo "Setting simple sitemap base url."
  #/app/vendor/bin/drush -y config:set simple_sitemap.settings base_url "https://${SITE_URL}"

  # Get site out of maintenance mode.
  /app/vendor/bin/drush state:set system.maintenance_mode 0 --input-format=integer
}
