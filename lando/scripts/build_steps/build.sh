#!/bin/bash

source /app/drups/scripts/drups_includes.sh
source /app/drups/scripts/drupal_build.sh
source /app/drups/config/drups_config

# Get current commit hash.
git_hash="$(git rev-parse HEAD)"

# Build Drupal.
drupal_build

# Set correct permissions for lando.
chown -R "${user_id}":"${group_id}" /app/vendor /app/web
