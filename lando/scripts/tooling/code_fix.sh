if [ ! "${drups_includes_loaded}" == 'true' ]; then
  source /app/drups/scripts/drups_includes.sh
fi

/app/vendor/bin/phpcbf -i

# Check if custom modules.
if ls "${drupal_root}"/modules/custom/*/*.module >/dev/null 2>&1; then
  echo -e "\nCustom modules found."

  # Fix Drupal coding standards.
  echo -e "\nFixing Drupal coding standards."
  phpcbf --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml --ignore=node_modules,bower_components,vendor "${drupal_root}/modules/custom"

  # Fix Drupal best practices.
  echo -e "\nFixing Drupal best practices."
  phpcbf --standard=DrupalPractice --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml --ignore=node_modules,bower_components,vendor "${drupal_root}/modules/custom"

else
  echo -e "\nNo custom modules found."
fi

# Check if custom themes.
if ls "${drupal_root}"/themes/custom/*/*.module >/dev/null 2>&1; then
  echo -e "\nCustom themes found."

  # Check Drupal coding standards.
  echo -e "\nChecking Drupal coding standards."
  phpcbf --standard=Drupal --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml --ignore=node_modules,bower_components,vendor "${drupal_root}/themes/custom"

  # Check Drupal best practices.
  echo -e "\nChecking Drupal best practices."
  phpcbf --standard=DrupalPractice --extensions=php,module,inc,install,test,profile,theme,css,info,txt,md,yml --ignore=node_modules,bower_components,vendor "${drupal_root}/themes/custom"

else
  echo -e "\nNo custom themes found."
fi
