#!/bin/bash
source /app/lando/scripts/tooling/tooling_includes.sh

# Check if kubeconfig exists.
check_cluster_accessible "${kube_context}"

# Destroy remote deployment.
helm --kubeconfig "${kube_config_file}" --kube-context "${kube_context}" uninstall "${namespace}-${pod_prefix}" -n "${namespace}"
