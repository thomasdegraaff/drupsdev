#!/bin/bash
source /app/lando/scripts/tooling/tooling_includes.sh

# Check if composer.json exists.
check_composer_json

# Set site in maintenance mode.
echo "Putting site in maintenance mode."
/app/vendor/bin/drush state:set system.maintenance_mode 1 --input-format=integer
check_result $?

echo "Clearing Drupal caches."
cd "${site_dir}"
/app/vendor/bin/drush cache:rebuild
check_result $?

# Store database.
echo "Storing database."
mysqldump -u drupal --password=drupal -h mariadb drupal | gzip -c > "${store_dir}/database-${git_hash}.gz"
check_result $?

# Store public files.
echo "Storing public files."
cd "${site_dir}"
tar -czf "${store_dir}/files-${git_hash}.tar.gz" ./files
check_result $?

# Store private files.
echo "Storing private files."
cd "/app/drupal"
tar -czf "${store_dir}/private-files-${git_hash}.tar.gz" ./private_files
check_result $?

# Get site out of maintenance mode.
echo "Getting site out of maintenance mode."
/app/vendor/bin/drush state:set system.maintenance_mode 0 --input-format=integer
check_result $?

if [ -f "${store_dir}/files-${git_hash}.tar.gz" ] &&  [ -f "${store_dir}/database-${git_hash}.gz" ]; then
  echo "Finished storing site data."
  ok
else
  error "Something went wrong when trying to store site data."
fi
