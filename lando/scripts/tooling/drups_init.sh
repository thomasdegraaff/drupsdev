#!/bin/bash
drups_init='true'
if [ -z "${site_dir}" ]; then
  source /app/lando/scripts/tooling/tooling_includes.sh
fi

# Set user.
user="$(whoami)"
user_id="$(id -u ${user})"
group_id="$(id -g ${user})"

# Set ssh private key file location.
ssh_private_key_file=/tmp/private_key

# Check for uncommitted changes.
check_uncommitted

# Check if kube config file exists.
check_kube_config_file

# Define secret types.
secret_types=(
  "drupal_admin_password"
  "drupal_hash_salt"
  "drupal_cron_key"
  "mariadb_password"
)

echo "----------------------------------------------------"
echo
echo "Answer some questions to initialize drups."
echo

# Get git repo.
readarray -t git_urls < <(git remote -v | grep 'fetch' | cut -f 1,2 | cut -d ' ' -f 1 | uniq)
urls=("${git_urls[@]##*[[:blank:]]}")
origins=("${git_urls[@]%%[[:blank:]]*}")
echo
if [ "${#urls[@]}" -eq "0" ]; then
  # No remote found.
  error "No remote set in git. Set a git remote, and try again."
elif [ "${#urls[@]}" -eq "1" ]; then
  # One remote found, ask for confirmation.
  echo "Git repo url: ${urls[0]}"
  echo
  read -p "Press [enter] to confirm, any other key to exit." -n 1 -r
  if [[ $REPLY != "" ]]; then
    echo
    error "Setup a git remote in the repository first, and then try again."
  else
    git_origin="${origins[0]}"
    git_url="${urls[0]}"
  fi
else
  quitindex="${#urls[@]}"
  urls+=("quit")
  echo "What remote repo do you want to use?"
  for index in "${!urls[@]}"; do
    echo "${index}) ${urls[$index]}"
  done
  answered='false'
  while [ $answered == 'false' ]; do
    read -p '' -rs -n1
    if [ "${REPLY}" == "${quitindex}" ]; then
      exit
    fi
    if [ ! -z "${origins[$REPLY]}" ]; then
      answered='true'
      git_origin="${origins[$REPLY]}"
      git_url="${urls[$REPLY]}"
    fi
  done
fi

# Get default git branch.
git_branch="$(git branch --show-current)"
git_branch="${git_branch,,}"

echo
echo "Git branch to work on: ${git_branch}"
read -p "Press [enter] to confirm." -n 1 -r
if [[ $REPLY != "" ]]; then
  echo
  error "Checkout the correct branch first."
fi

# Get private key if private repo.
ssh_private_key=''
if [[ "${git_url}" == *git@* ]]; then
  correct=false
  while [ "$correct" == 'false' ]; do
    echo
    echo "Paste the private key used to access the git repo:"
    echo "(do not press enter, wait a few seconds to process)"
    OLDIFS=$IFS
    IFS= read -d '' -n 1 ssh_private_key
    while IFS= read -d '' -n 1 -t 1 c; do
      ssh_private_key+=$c
    done
    IFS=$OLDIFS
    echo "${ssh_private_key[@]}" > "${ssh_private_key_file}"
    chmod 600 "${ssh_private_key_file}"
    export GIT_SSH_COMMAND="ssh -i ${ssh_private_key_file} -o IdentitiesOnly=yes -o StrictHostKeyChecking=no  -o UserKnownHostsFile=/dev/null"
    git ls-remote "git@gitlab.com:thomasdegraaff/ds-procoid.tomio.nl.git" > /dev/null 2>&1
    if [ $? -eq 0 ]; then
      echo -e "\n\nSuccessfully authenticated."
      ok
      correct=true
      ssh_private_key="${ssh_private_key[@]}"
    else
      error "Could not authenticate to the chosen git repo with the provided key."
    fi
  done
  export GIT_SSH_COMMAND="ssh -i ${ssh_private_key_file} -o IdentitiesOnly=yes -o StrictHostKeyChecking=no  -o UserKnownHostsFile=/dev/null"
fi

# Check if current branch up-to-date with remote repo.
check_local_and_remote_repo_sync

# Ask for client name.
echo
read -e -p "Client name: " -i "${default_client}" client_name
client_name="${client_name//[^a-z0-9]/}"

# Get project name.
project_name="${git_url##*/}"
project_name="${project_name%%.git}"
project_name="${project_name//[^a-z0-9]/}"
read -e -p "Project name: " -i "${project_name}" project_name
project_name="${project_name//[^a-z0-9]/}"

# Show naming convention message.
cat <<EOF

The project will be deployed in Kubernetes. The project namespace should be unique for each project. The pod prefix should be unique within the namespace, and correspond to a branch. To make sure this is the case the default setting for the pod prefix is the git branch name.

Use alphanumeric lowercase characters and dashes only for the namespace and pod prefix.

Press any key to continue...
EOF

read -n 1 -r

# Get namespace.
namespace="${client_name,,}-${project_name,,}"
namespace="${namespace//[^a-z0-9-]/}"
correct=false
while [ "$correct" == 'false' ]; do
  read -e -p "Project namespace: " -i "${namespace}" namespace
  if [ "${namespace}" == "${namespace//[^a-z0-9-]/}" ]; then
    correct=true
  else
    echo
    echo "Use only alphanumerical characters and dash."
    echo
  fi
done

# Get context.
echo -e "\nSelect Kubernetes context to use for site deployment."
get_context
kube_context="${context}"

# Test if cluster is up and accessible.
check_cluster_accessible "${kube_context}"

# Get storage class.
echo
readarray -t storage_classes < <(kubectl --kubeconfig "${kube_config_file}" --context "${kube_context}" get storageClass -o json | jq -r .items[].metadata.name)
if [ "${#storage_classes[@]}" -eq "0" ]; then
  # No storage class found.
  error "No kubernetes storage class found."
elif [ "${#storage_classes[@]}" -eq "1" ]; then
  storage_class="${storage_classes[0]}"
else
  quitindex="${#storage_classes[@]}"
  storage_classes+=("quit")
  echo "What Kubernetes storage class do you want to use?"
  for index in "${!storage_classes[@]}"; do
    echo "${index}) ${storage_classes[$index]}"
  done
  answered='false'
  while [ $answered == 'false' ]; do
    read -p '' -rs -n1
    if [ "${REPLY}" == "${quitindex}" ]; then
      exit
    fi
    if [ ! -z "${storage_classes[$REPLY]}" ]; then
      answered='true'
      storage_class="${storage_classes[$REPLY]}"
    fi
  done
fi

# Get access mode.
case "${storage_class}" in

  'local-path')
    storage_access_mode="ReadWriteOnce"
    ;;

  'longhorn')
    storage_access_modes=("ReadWriteOnce" "ReadWriteMany")
    ;;

  'do-block-storage')
    storage_access_mode="ReadWriteOnce"
    ;;

  *)
    storage_access_modes=("ReadWriteOnce" "ReadWriteMany")
    ;;
esac
if [ -z "${storage_access_mode}" ]; then
  storage_access_modes+=("quit")
  quitindex="${#storage_access_modes[@]}"
  echo -e "\nWhat Kubernetes storage access mode do you want to use?"
  for index in "${!storage_access_modes[@]}"; do
    echo "${index}) ${storage_access_modes[$index]}"
  done
  answered='false'
  while [ $answered == 'false' ]; do
    read -p '' -rs -n1
    if [ "${REPLY}" == "${quitindex}" ]; then
      exit
    fi
    if [ ! -z "${storage_access_modes[$REPLY]}" ]; then
      answered='true'
      storage_access_mode="${storage_access_modes[$REPLY]}"
    fi
  done
fi

# Get storage sizes.
echo
echo "What storage sizes do you want?"
read -e -p "Code storage size: " -i "${code_storage_size}" code_storage_size
read -e -p "Database storage size: " -i "${database_storage_size}" database_storage_size
read -e -p "Files storage size: " -i "${files_storage_size}" files_storage_size
read -e -p "Private files storage size: " -i "${private_files_storage_size}" private_files_storage_size

if [ "${storage_access_mode}" == 'ReadWriteOnce' ]; then
  # Get schedule node.
  readarray -t nodes < <(kubectl --kubeconfig "${kube_config_file}" --context "${kube_context}" get node -o json | jq -r .items[].metadata.name)
  if [ "${#nodes[@]}" -eq "0" ]; then
    # No node found.
    error "No node found in cluster."
  elif [ "${#nodes[@]}" -eq "1" ]; then
    node_name="${nodes[0]}"
  else
    quitindex="${#nodes[@]}"
    nodes+=("quit")
    echo -e "\nWhat node do you want schedule storage using pods to?"
    for index in "${!nodes[@]}"; do
      echo "${index}) ${nodes[$index]}"
    done
    answered='false'
    while [ $answered == 'false' ]; do
      read -p '' -rs -n1
      if [ "${REPLY}" == "${quitindex}" ]; then
        exit
      fi
      if [ ! -z "${nodes[$REPLY]}" ]; then
        answered='true'
        node_name="${nodes[$REPLY]}"
      fi
    done
  fi
else
  node_name=""
fi

# Check if deployment exists.
existing_deployment="$(kubectl --kubeconfig "${kube_config_file}" --context "${kube_context}" get --namespace="${namespace}" deployment -o json | jq -r ".items[] | select (.metadata.labels.branch == \"${git_branch}\" and .metadata.labels.type == \"php\")")"
if [ -z "$existing_deployment" ]; then
  # No existing deployment found.
  echo
  echo "No existing deployment found."

  # Get pod_prefix.
  pod_prefix="${git_branch//[^a-z0-9-]/}"
  echo
  correct=false
  while [ "$correct" == 'false' ]; do
    read -e -p "Pod_prefix: " -i "${pod_prefix}" pod_prefix
    if [ "${pod_prefix}" == "${pod_prefix//[^a-z0-9-]/}" ]; then
      correct=true
    else
      echo
      echo "Use only alphanumerical characters and dash."
      echo
    fi
  done

  # Get deployment domain.
  echo
  correct=false
  domain="${default_domain}"
  while [ "$correct" == 'false' ]; do
    read -e -p "Deployment domain: " -i "${domain}" domain
    if [[ $domain =~ ^([a-zA-Z0-9](([a-zA-Z0-9-]){0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,}$ ]]; then
      correct=true
    else
      echo
      echo "Set a correct domain, something like 'drups.eu'."
      echo
    fi
  done

  # Get deployment subdomain.
  subdomain="${pod_prefix}.${project_name}"
  correct=false
  while [ "$correct" == 'false' ]; do
    read -e -p "Deployment subdomain (may be empty): " -i "${subdomain}" subdomain
    if [[ $subdomain =~ ^([a-zA-Z0-9](([a-zA-Z0-9-]){0,61}[a-zA-Z0-9])?\.?)*$ ]]; then
      correct=true
    else
      echo
      echo "Set a correct subdomain, something like 'drups.eu'."
      echo
    fi
  done

  # Get source context.
  echo
  while true; do
    read -p "Do you want to import data from another deployment? (y/n)" -rs -n1 yn
    case $yn in
      [yY] )
        echo -e "\nSelect Kubernetes context to import data from."
        get_context
        source_context="${context}"
        if [ ! "${source_context}" = "${kube_context}" ]; then
          check_cluster_accessible "${source_context}"
        fi
        # Get source branch if available.
        kube_deployments="$(kubectl --kubeconfig "${kube_config_file}" --context "${source_context}" get deployment --namespace="${namespace}" -o json | jq -r '.items[] | select (.metadata.labels.type == "php")')"
        kube_branches="$(echo -n "${kube_deployments}" | jq -r '.metadata.labels.branch')"
        if [ ! -z "${kube_branches}" ]; then
          # Get branch.
          echo -e  "\nSelect the deployed branch to import site data from."
          options="${kube_branches} quit"
          select opt in $options ; do
            if [ "$opt" == "quit" ]; then
              exit 0
            else
              break
            fi
          done
          source_branch=${opt%/}
          deployment_name="$(echo "${kube_deployments}" | jq -r "select (.metadata.labels.branch == \"${source_branch}\") | .metadata.name")"
          source_prefix="${deployment_name%-php}"
          echo
          echo "Source branch deployment: $source_branch"
        else
          error "No deployment found in context '${source_context}'."
        fi
        break
        ;;
      [nN] )
        source_context="${kube_context}"
        # Get passwords from user.
        echo -e "\n\nSet secrets for Kubernetes deployment."
        for secret_type in ${secret_types[@]}; do
          if [ -z "${!secret_type}" ]; then
            default_value="$(pwgen -n 16)"
          else
            default_value="${!secret_type}"
          fi
          label="$(echo "${secret_type^}" | sed 's/_/ /g')"
          read -e -p "${label}: " -i "${default_value}" "${secret_type}"
        done
        break
        ;;
    esac
  done
else
  # Existing deployment found.
  echo
  echo "Found existing deployment, importing pod prefix and secrets."
  site_url="$(kubectl --kubeconfig "${kube_config_file}" --context "${kube_context}" get --namespace="${namespace}" ingress -o json | jq -r "[.items[] | select (.metadata.labels.branch == \"${git_branch}\")][0].spec.rules[0].host")"
  subdomain="${site_url%.*.*}"
  if [ "$subdomain" == "$site_url" ]; then
    domain="${subdomain}"
    subdomain=''
  else
    domain="${site_url/${subdomain}./}"
  fi
  source_branch=${git_branch}
  deployment_name="$(echo "${existing_deployment}" | jq -r '.metadata.name')"
  pod_prefix="${deployment_name%-php}"
  source_prefix="${pod_prefix}"
  source_context="${kube_context}"
  echo
  echo "Source branch deployment: $source_branch"
fi

# Set lando name.
lando_name="${namespace}-${pod_prefix}"

if [ ! -z "$source_branch" ]; then
  # Get passwords from cluster.
  drups_secrets="$(kubectl --kubeconfig "${kube_config_file}" --context "${source_context}" get secret --namespace="${namespace}" "${source_prefix}-drups-secrets" -o json | jq '.data')"
  drupal_admin_password="$(echo "$drups_secrets" | jq -r '."drupal-admin-password"' | base64 -d)"
  drupal_hash_salt="$(echo "$drups_secrets" | jq -r '."drupal-hash-salt"' | base64 -d)"
  drupal_cron_key="$(echo "$drups_secrets" | jq -r '."drupal-cron-key"' | base64 -d)"
  mariadb_password="$(echo "$drups_secrets" | jq -r '."mariadb-password"' | base64 -d)"
  basic_authentication_user="$(echo "$drups_secrets" | jq -r '."basic-authentication-user"' | base64 -d)"
  basic_authentication_password="$(echo "$drups_secrets" | jq -r '."basic-authentication-password"' | base64 -d)"
  mailhog_user="$(echo "$drups_secrets" | jq -r '."mailhog-user"' | base64 -d)"
  mailhog_password="$(echo "$drups_secrets" | jq -r '."mailhog-password"' | base64 -d)"

  # Get source url.
  source_url="$(kubectl --kubeconfig "${kube_config_file}" --context "${source_context}" get --namespace="${namespace}" ingress -o json | jq -r ".items[] | select (.metadata.labels.branch == \"${source_branch}\") | .spec.rules[0].host")"
fi

if [ -z "$existing_deployment" ]; then
  # Get basic authentication.
  echo
  echo "Basic authentication (leave empty to disable basic authentication)"
  read -e -p "user: " -i "${basic_authentication_user}" basic_authentication_user
  read -e -p "password: " -i "${basic_authentication_password}" basic_authentication_password
  echo
  echo "Mailhog authentication (leave empty to disable mailhog)"
  read -e -p "user: " -i "${mailhog_user}" mailhog_user
  read -e -p "password: " -i "${mailhog_password}" mailhog_password
fi

# Set site url and lando default subdomain.
if [ -z "${subdomain}" ]; then
  site_url="${domain}"
  default_lando_subdomain="${domain%.*}"
else
  site_url="${subdomain}.${domain}"
  default_lando_subdomain="${subdomain}"
fi

echo
read -e -p "Lando subdomain: " -i "${default_lando_subdomain}" lando_subdomain
lando_url="${lando_subdomain}.lndo.site"

# Show values for confirmation.
cat <<EOF



Check if the following is ok.

General
-------
project repo: ${git_url}
branch: ${git_branch}

Remote deployment
-----------------
kubernetes context: ${kube_context}
node name: ${node_name}
storage class: ${storage_class}
storage access mode: ${storage_access_mode}
database storage size: ${database_storage_size}
files storage size: ${files_storage_size}
private files storage size: ${private_files_storage_size}
code storage size: ${code_storage_size}

site url: https://${site_url}
namespace: ${namespace}
pod prefix: ${pod_prefix}
EOF

if [ ! -z "${source_branch}" ]; then
  echo -e "\nsource context: ${source_context}"
  echo "source branch: ${source_branch}"
fi

cat <<EOF

drupal:
admin password: ${drupal_admin_password}
cron key: ${drupal_cron_key}
hash salt: ${drupal_hash_salt}
mariadb password: ${mariadb_password}

basic authentication:
user: ${basic_authentication_user}
password: ${basic_authentication_password}

mailhog:
user: ${mailhog_user}
password: ${mailhog_password}

Lando
-----
local url: https://${lando_url}
EOF

if [ ! -z "${source_branch}" ]; then
  echo "data source: ${source_branch} branch deployment (https://${source_url})"
fi

cat <<EOF

secrets:
drupal admin password: ${lando_drupal_admin_password}
drupal hash salt: ${lando_drupal_hash_salt}
drupal cron key: ${lando_drupal_cron_key}
mariadb password: ${lando_mariadb_password}
EOF

echo
read -p "Press [enter] to confirm, any other key to quit." -n 1 -r
echo
if [[ $REPLY != "" ]]; then
  exit 0
fi

# Store drups config and create lando and helm config files.
create_config

# Set deployment tasks.
if [ -z "${source_branch}" ]; then
  # New branch, no source branch.
  deploy_site='true'
  deploy_data='false'
else
  if [ "${source_branch}" == "${git_branch}" ]; then
    # Existing branch.
    deploy_site='false'
    deploy_data='false'
  else
    # New branch based on existing branch.
    deploy_site='true'
    deploy_data='true'
  fi
fi

# Deploy.
if [ "${deploy_site}" == 'true' ]; then
  # Deploy new site in Kubernetes.
  echo "Deploying ${git_branch} branch in Kubernetes."
  helm --kubeconfig  "${kube_config_file}" --kube-context "${kube_context}" install --wait --create-namespace -n "${namespace}" "${namespace}-${pod_prefix}" /app/kube/helm/drups
  check_result $?

  # Wait until site deployment is ready.
  wait_for_drupal_pod

  # Build Drupal.
  deploy_build_drupal
fi

# Get deployment pod names.
echo "Getting deployment pod names."
echo 'drupal-tools'
deploy_pod_name="$(get_pod_name "${pod_prefix}" 'drupal-tools' "${kube_context}")"
check_result $?
echo 'mariadb'
deploy_mariadb_pod_name="$(get_pod_name "${pod_prefix}" 'mariadb' "${kube_context}")"
check_result $?

# Install or update Drupal.
if [ "${deploy_site}" == 'true' ]; then
  if [ -z "${source_branch}" ]; then
    # Wait until database ready.
    echo "Waiting until database is ready."
    execute_job "${deploy_pod_name}" 'drupal-tools' '/app/drups/scripts/check_database.sh' "${kube_context}"
    check_result $?

    echo "Installing or updating Drupal."
    execute_job "${deploy_pod_name}" 'drupal-tools' '/app/kube/scripts/drupal_install.sh' "${kube_context}"
    check_result $?
  fi
fi

# Download deployment code changes.
if [ -z "${source_branch}" ]; then
  # Composer.json.
  echo "Downloading composer composer.json."
  execute_job "${deploy_pod_name}" 'drupal-tools' "cat /app/composer.json" > "/app/composer.json" "${kube_context}"
  check_result $?

  # Composer.lock.
  echo "Downloading composer composer.lock."
  execute_job "${deploy_pod_name}" 'drupal-tools' "cat /app/composer.lock" > "/app/composer.lock" "${kube_context}"
  check_result $?

  # Get config.
  echo "Export config in deployment ${git_branch} branch deployment."
  execute_job "${deploy_pod_name}" 'drupal-tools' '/app/vendor/bin/drush config:export' "${kube_context}"
  check_result $?

  echo "Download config from ${git_branch} branch deployment."
  execute_job "${deploy_pod_name}" 'drupal-tools' "cd ${drupal_config_dir} && tar -czf - ." "${kube_context}" | tar -C "${drupal_config_dir}" -xzf -
  check_result $?
fi

# Prepare for downloading data from deployment.
if [ -z "${source_branch}" ]; then
  data_branch="${git_branch}"
  data_prefix="${pod_prefix}"
  data_url="${site_url}"
else
  data_branch="${source_branch}"
  data_prefix="${source_prefix}"
  data_url="${source_url}"
fi

# Set source deployment in maintenance mode.
set_maintenance_mode "${data_prefix}" "${data_url}" 1 "${source_context}"

# Download data from deployment.
pull_data "${data_branch}" "${data_prefix}" "${source_context}"

# Get source deployment out of maintenance mode.
set_maintenance_mode "${data_prefix}" "${data_url}" 0 "${source_context}"

if [ "${deploy_data}" == 'true' ]; then
  # Push data.
  push_data
fi

# Clear varnish cache.
varnish_cache_clear "${git_branch}" "${pod_prefix}" "${kube_context}"

cat <<EOF
Drups is initialized.

Remote deployment
-----------------
url: https://${site_url}/user/login

EOF

if [ ! -z "${basic_authentication_user}" ] && [ ! -z "${basic_authentication_password}" ]; then
cat <<EOF
basic authentication:
user: ${basic_authentication_user}
password: ${basic_authentication_password}

EOF
fi

cat <<EOF
drupal:
user: admin
password: ${drupal_admin_password}

EOF

if [ ! -z "${mailhog_user}" ] && [ ! -z "${mailhog_password}" ]; then
cat << EOF
mailhog:
url: https://mailhog.${site_url}
user: ${mailhog_user}
password: ${mailhog_password}

EOF
fi

cat << EOF
Local deployment (not running yet)
----------------------------------
url: https://${lando_url}/user/login

drupal:
user: admin
password: drupal

mailhog:
url: https://mailhog.${lando_url}

Now execute 'lando start' to startup the local deployment.
EOF
