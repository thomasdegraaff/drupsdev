#!/bin/bash
if [ ! "${drups_includes_loaded}" == 'true' ]; then
  source /app/drups/scripts/drups_includes.sh
fi

# Check if composer.json exists.
check_composer_json

# Import drupal config.
echo
echo "Importing Drupal configuration."
cd "${site_dir}" && /app/vendor/bin/drush --root=/app config:import
echo
echo "Finished importing Drupal configuration."
