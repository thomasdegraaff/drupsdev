#!/bin/bash
source /app/lando/scripts/tooling/tooling_includes.sh

# Check if kubeconfig exists.
check_cluster_accessible "${kube_context}"

# Check if deployment is ok.
check_deployment

# Set deployment in maintenance mode.
set_maintenance_mode "${pod_prefix}" "${site_url}" 1 "${kube_context}"

# Download data from deployment.
pull_data "${git_branch}" "${pod_prefix}" "${kube_context}"

# Get target deployment out of maintenance mode.
set_maintenance_mode "${pod_prefix}" "${site_url}" 0 "${kube_context}"
