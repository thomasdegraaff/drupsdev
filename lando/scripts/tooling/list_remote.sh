#!/bin/bash
source /app/lando/scripts/tooling/tooling_includes.sh

# Check if kubeconfig exists.
check_cluster_accessible "${kube_context}"

helm --kubeconfig "${kube_config_file}" --kube-context "${kube_context}" list -n "${namespace}"
