#!/bin/bash
if [ ! "${drups_includes_loaded}" == 'true' ]; then
  source /app/drups/scripts/drups_includes.sh
fi

# Set kube config file.
kube_config_file="$HOME/.kube/config"

# Get current commit hash.
git_hash="$(git rev-parse HEAD)"

if [ ! "${drups_init}" == 'true' ]; then
  # Import drups config.
  if [ -f /app/drups/config/drups_config ]; then
    source /app/drups/config/drups_config
  else
    error "Drups project is not initialized yet. Execute drups-init in the project folder to initialize."
  fi

  # Set private key file.
  if [[ "${git_url}" == *git@* ]]; then
    echo "Storing private key."
    ssh_private_key_file=/tmp/lando_private_key
    echo "${ssh_private_key}" > "${ssh_private_key_file}"
    chmod 600 "${ssh_private_key_file}"
    export GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o IdentitiesOnly=yes -i ${ssh_private_key_file}"
  fi
fi

declare -A pod_names
declare -A vars=(
  # Variables.
  [namespace]="namespace"
  [pod_prefix]="podPrefix"
  [git_url]="gitUrl"
  [git_branch]="gitBranch"
  [git_origin]="gitOrigin"
  [site_url]="siteUrl"
  [varnish_size]="varnishSize"
  [project_name]="projectName"
  # Php settings.
  [php_memory_limit_build]="phpMemoryLimitBuild"
  [php_memory_limit_tooling]="phpMemoryLimitTooling"
  [php_memory_limit_fpm]="phpMemoryLimitFpm"

  [php_max_execution_time_build]="phpMaxExecutionTimeBuild"
  [php_max_execution_time_tooling]="phpMaxExecutionTimeTooling"
  [php_max_execution_time_fpm]="phpMaxExecutionTimeFpm"

  [php_upload_max_filesize_build]="phpUploadMaxFilesizeBuild"
  [php_upload_max_filesize_tooling]="phpUploadMaxFilesizeTooling"
  [php_upload_max_filesize_fpm]="phpUploadMaxFilesizeFpm"

  [php_post_max_size_build]="phpPostMaxSizeBuild"
  [php_post_max_size_tooling]="phpPostMaxSizeTooling"
  [php_post_max_size_fpm]="phpPostMaxSizeFpm"
  # Deployment secrets.
  [drupal_admin_password]="drupalAdminPassword"
  [drupal_hash_salt]="drupalHashSalt"
  [drupal_cron_key]="drupalCronKey"
  [mariadb_password]="mariadbPassword"
  # Basic authentication
  [basic_authentication_user]="basicAuthenticationUser"
  [basic_authentication_password]="basicAuthenticationPassword"
  # Mailhog
  [mailhog_user]="mailhogUser"
  [mailhog_password]="mailhogPassword"
  # Node name.
  [node_name]="nodeName"
  # Storage.
  [storage_class]="codeStorageClass"
  [storage_access_mode]="codeStorageAccessMode"
  [code_storage_size]="codeStorageSize"
  [files_storage_size]="filesStorageSize"
  [private_files_storage_size]="privateFilesStorageSize"
  [database_storage_size]="databaseStorageSize"
  # Drups images.
  [drupal_tools_image]="drupalToolsImage"
  [nginx_image]="nginxImage"
  [php_image]="phpImage"
  [varnish_image]="varnishImage"
  [redis_image]="redisImage"
  [mailhog_image]="mailhogImage"
  [mariadb_image]="mariadbImage"
  # Content security policy.
  [content_security_policy]="contentSecuriyPolicy"
  # Lando.
  [kube_context]=""
  [lando_name]=""
  [lando_url]=""
  [lando_drupal_admin_password]=""
  [lando_drupal_cron_key]=""
  [lando_drupal_hash_salt]=""
  [lando_mariadb_password]=""
  [lando_tools_image]=""
  [lando_php_image]=""
  [kube_config_file]=""
  [user]=""
  [user_id]=""
  [group_id]=""
  [xdebug_port]=""
)

function get_drupal_tools_deployment() {
  # $1: deployment_branch
  # $2: context
  if [ -z "${1}" ] || [ -z "${2}" ]; then
    error
  else
    echo "$(kubectl --kubeconfig "${kube_config_file}" --context "${2}" get deployment --namespace="${namespace}" -o json | jq ".items[] | select (.metadata.labels.branch == \"${1}\" and .metadata.labels.type == \"drupal-tools\" and .status.availableReplicas == 1)")"
  fi
}

function check_kube_config_file() {
  if [ ! -f "${kube_config_file}" ]; then
    error "Kube config file not found. Make sure the kube config can be found in your home folder in .kube/.config ."
  fi
}

function check_cluster_accessible() {
  # $1: context

  # Check if context variable is set.
  if [ -z "${1}" ]; then
    error 'Kubernetes context variable not set. Re-initialize Drups.'
  fi

  # Check if kube config file exists.
  check_kube_config_file

  # Check if context exsits.
  kubectl config get-contexts | grep -v 'AUTHINFO' | grep "${1}" > /dev/null 2>&1
  if [ ! $? -eq 0 ]; then
    error "Couldn't find Kubernetes context '${1}'."
  fi

  # Check if cluster is accessible.
  kubectl --kubeconfig "${kube_config_file}" --context "${1}" get nodes | grep -'Ready' > /dev/null 2>&1
  if [ ! $? -eq 0 ]; then
    error "Couldn't access cluster '${1}'."
  fi

  if [ ! "${drups_init}" == 'true' ]; then
    # Check if project namespace exists.
    kubectl --kubeconfig "${kube_config_file}" --context "${1}" get ns "${namespace}"
    if [ ! $? -eq 0 ]; then
      error "Couldn't find project namespace '${namespace}' in context '${1}'."
    fi
  fi

  # Check if nodes exist.
  nodes=$(kubectl --kubeconfig "${kube_config_file}" --context "${1}" get node -o json | jq '.items | length')
  if [ "${nodes}" -lt 1 ]; then
    error "No nodes found in context '${1}'."
  fi
}

function check_deployment() {
  # Check deployment.
  echo "Checking if Drupal tools deployment '${git_branch}' is available."
  drupal_tools_deployment="$(get_drupal_tools_deployment "${git_branch}" "${kube_context}")"
  if [ -z "${drupal_tools_deployment}" ]; then
    error "Deployment not found."
  else
    ok
  fi
}

function check_source_deployment() {
  # Check if source deployment.
  if [ ! -z "${source_branch}" ] && [ ! "${source_branch}" == "${git_branch}" ]; then
    echo "Checking Drupal tools deployment '${source_branch}' is available."
    drupal_tools_source_deployment="$(get_drupal_tools_deployment "${source_branch}" "${kube_context}")"
    if [ -z "${drupal_tools_source_deployment}" ]; then
      error "Deployment '${source_branch}' not found."
    else
      ok
    fi
  fi
}

function check_local_and_remote_repo_sync() {
  # Check if current branch up-to-date with remote repo.
  echo "Checking if local and remote branch are up-to-date."
  GIT_TERMINAL_PROMPT=0 git fetch "${git_origin}" > /dev/null 2>&1
  check_result $? "Couldn't fetch git remote ${git_origin}. Use a git@... repository when the repository is password protected."
  if [ "$(git rev-parse HEAD)" != "$(git rev-parse "${git_origin}/${git_branch}")" ]; then
    error "The local and remote git branch are out of sync."
  else
    ok
  fi
}

function wait_for_drupal_pod() {
  echo "Waiting until Drupal deployment is ready."
  drupal_pod_ready='false'
  while [ "$drupal_pod_ready" == 'false' ]; do
    echo -n '.'
    sleep 5
    available_deployment="$(kubectl --kubeconfig "${kube_config_file}" --context "${kube_context}" get deployment --namespace="${namespace}" -o json | jq ".items[] | select (.metadata.labels.branch == \"${git_branch}\" and .metadata.labels.type == \"drupal-tools\" and .status.availableReplicas == 1)")"
    if [ ! -z "$available_deployment" ]; then
      drupal_pod_ready='true'
      echo
    fi
  done
  ok
}

function get_pod_name() {
  # Arguments.
  # $1: pod prefix
  # $2: pod_type
  # $3: context

  # Check if already known.
  name_index="${1}-${2}"
  if [ ! "${pod_names["${name_index+_}"]}" ]; then
    p_name="$(kubectl --kubeconfig "${kube_config_file}" --context "${3}" get pod --namespace="${namespace}" -o json | jq -r "[.items[] | select (.metadata.labels.name == \"${name_index}\")][0] | .metadata.name")"
    if [ ! $? -eq 0 ]; then
      error "Couldn't retrieve ${name_index} pod name."
    fi
    pod_names["${name_index}"]="${p_name}"
  fi
  # Output pod name.
  echo "${p_name}"
}

function execute_job() {
  # Arguments.
  # $1: pod_name
  # $2: container_name
  # $3: job_command
  # $4: context
  # Execute command.
  kubectl --kubeconfig "${kube_config_file}" --context "${4}" exec --namespace="${namespace}" -i "${1}" -c "${2}" -- /bin/sh -c "${3}"
  return $?
}

function deploy_build_drupal() {
  # Prepare build job.
  echo "Preparing build job."
  cp "/app/kube/jobs/drupal-build.yaml.orig" "/app/kube/jobs/drupal-build.yaml"
  for var in "${!vars[@]}"; do
    if [ ! -z "${vars[$var]}" ]; then
      sed -i "s~!${var}!~${!var}~g" "/app/kube/jobs/drupal-build.yaml"
    fi
  done
  random_string="$(echo $RANDOM | md5sum | head -c 6)"
  sed -i "s~!random_string!~${random_string}~g" "/app/kube/jobs/drupal-build.yaml"
  ok

# Build Drupal.
  echo "Creating the build job."
  kubectl --kubeconfig "${kube_config_file}" --context "${kube_context}" apply -f /app/kube/jobs/drupal-build.yaml
  check_result $?

# Wait until Drupal build is ready.
  echo "Waiting until job ${pod_prefix}-drupal-build-${random_string} is ready."
  while [ ! "$(kubectl --kubeconfig "${kube_config_file}" --context "${kube_context}" get job --namespace="${namespace}" "${pod_prefix}-drupal-build-${random_string}" -o json | jq .status.succeeded)" == '1' ]; do
    echo -n '.'
    sleep 5
  done
  echo
  ok
}

function push_data() {

  # Get drupal tools pod name.
  drupal_tools_pod_name="$(get_pod_name "${pod_prefix}" 'drupal-tools' "${kube_context}")"

  # Get remote git commit hash.
  echo "Getting remote git commit hash."
  git_hash_remote=$(execute_job "${drupal_tools_pod_name}" 'drupal-tools' 'git rev-parse HEAD' "${kube_context}")

  # Check if storage exists.
  echo "Look for data storage that corresponds with remote git commit (${git_hash_remote})."
  if [ -f "${store_dir}/files-${git_hash_remote}.tar.gz" ] &&  [ -f "${store_dir}/database-${git_hash_remote}.gz" ] &&  [ -f "${store_dir}/private-files-${git_hash_remote}.tar.gz" ]; then
    ok
  else
    error "No data storage corresponding to remote git commit ${git_hash_remote} found."
  fi

  # Push public files to deployment.
  echo "Pushing public files to ${git_branch} branch deployment."
  execute_job "${drupal_tools_pod_name}" 'drupal-tools' "rm -rf ${site_dir}/files" "${kube_context}"
  cat "${store_dir}/files-${git_hash_remote}.tar.gz" | execute_job "${drupal_tools_pod_name}" 'drupal-tools' "tar -xzf - -C ${site_dir} && chown -R www-data:www-data ${site_dir}/files" "${kube_context}"
  check_result $?

  # Push private files to deployment.
  echo "Pushing private files to ${git_branch} branch deployment."
  execute_job "${drupal_tools_pod_name}" 'drupal-tools' "rm -rf /app/drupal/private_files" "${kube_context}"
  cat "${store_dir}/private-files-${git_hash_remote}.tar.gz" | execute_job "${drupal_tools_pod_name}" 'drupal-tools' "tar -xzf - -C /app/drupal && chown -R www-data:www-data /app/drupal/private_files" "${kube_context}"
  check_result $?

  # Push database to deployment.
  echo "Pushing database to ${git_branch} branch deployment."
  mariadb_pod_name="$(get_pod_name "${pod_prefix}" 'mariadb' "${kube_context}")"
  execute_job "${mariadb_pod_name}" 'mariadb' "mysql -u drupal -p${mariadb_password} -e 'DROP DATABASE drupal; CREATE DATABASE drupal;'" "${kube_context}"
  cat "${store_dir}/database-${git_hash_remote}.gz" | gzip -d | execute_job "${mariadb_pod_name}" 'mariadb' "mysql -u drupal -p${mariadb_password} drupal" "${kube_context}"
  check_result $?

  # Update Drupal.
  echo "Running updates."
  execute_job "${drupal_tools_pod_name}" 'drupal-tools' '/app/kube/scripts/drupal_install.sh' "${kube_context}"
  check_result $?
}

function pull_data() {
  # $1: branch
  # $2: pod prefix
  # $3: context

  # Get drupal tools pod name.
  drupal_tools_pod_name="$(get_pod_name "${2}" 'drupal-tools' "${3}")"
  echo "drupal tools podname: $drupal_tools_pod_name"

  # Get remote git commit hash.
  echo "Getting remote git commit hash."
  git_hash_remote="$(execute_job "${drupal_tools_pod_name}" 'drupal-tools' 'git rev-parse HEAD' "${3}")"

  # Download database from remote deployment.
  rm -f "${store_dir}/database-${git_hash_remote}.gz"
  echo "Download database from ${1} branch deployment."
  pod_name="$(get_pod_name "${2}" 'mariadb' "${3}")"
  execute_job "${pod_name}" 'mariadb' "mysqldump -u drupal -p${mariadb_password} drupal" "${3}" | gzip -c > "${store_dir}/database-${git_hash_remote}.gz"
  check_result $?
  [ -f "${store_dir}/database-${git_hash_remote}.gz" ] && ok || error

# Download public files from remote deployment.
  rm -f "${store_dir}/files-${git_hash_remote}.tar.gz"
  echo "Download public files from ${1} branch deployment."
  execute_job "${drupal_tools_pod_name}" 'drupal-tools' "cd ${site_dir} && tar -czf - files" "${3}" > "${store_dir}/files-${git_hash_remote}.tar.gz"
  check_result $?
  [ -f "${store_dir}/files-${git_hash_remote}.tar.gz" ] && ok || error

# Download private files from remote deployment.
  rm -f "${store_dir}/private-files-${git_hash_remote}.tar.gz"
  echo "Download private files from ${1} branch deployment."
  execute_job "${drupal_tools_pod_name}" 'drupal-tools' "cd /app/drupal && tar -czf - private_files" "${3}" > "${store_dir}/private-files-${git_hash_remote}.tar.gz"
  check_result $?
  [ -f "${store_dir}/private-files-${git_hash_remote}.tar.gz" ] && ok || error
}

function set_maintenance_mode() {
  # $1: pod_prefix
  # $2: url
  # $3: state
  # $4: context
  # Set source deployment in maintenance mode.

  if [ -z "${4}" ]; then
    error "Context not set."
  fi
  if [ "${3}" -eq 1 ]; then
    echo "Putting ${2} site in maintenance mode."
  else
    echo "Getting ${2} site out of maintenance mode."
  fi
  pod_name="$(get_pod_name "${1}" 'drupal-tools' "${4}")"
  execute_job "${pod_name}" 'drupal-tools' ". /app/drups/scripts/drups_includes.sh && check_composer_json && /app/vendor/bin/drush state:set system.maintenance_mode ${3}  --input-format=integer" "${4}"
  check_result $?
}

function varnish_cache_clear() {
  # $1: branch
  # $2: pod prefix
  # $3: context

  echo "Clearing varnish cache for ${1} branch deployment."
  pod_name="$(get_pod_name "${2}" 'varnish' "${3}")"
  execute_job "${pod_name}" 'varnish' 'varnishadm "ban req.url ~ ."' "${3}"
}

function check_uncommitted() {
  # Check for uncommitted changes.
  git add . && git diff --quiet && git diff --cached --quiet
  if [ ! $? -eq 0 ]; then
    echo -e "Uncommitted local changes found.\n"
    read -p "Press [enter] to continue, any other key to stop." -n 1 -r
    if [[ $REPLY != "" ]]; then
      error
    fi
  fi
}

function create_config() {
  # Store config.
  echo "Storing config."
  mkdir -p /app/drups/config
  cat <<EOF > "/app/drups/config/drups_config"
# General.
client_name='${client_name}'
project_name='${project_name}'
lando_name='${lando_name}'

# Url's.
domain='${domain}'
subdomain='${subdomain}'
site_url='${site_url}'
source_url='${source_url}'
lando_subdomain='${lando_subdomain}'
lando_url='${lando_url}'

# Git.
git_url='${git_url}'
git_origin='${git_origin}'
git_branch='${git_branch}'
source_branch='${source_branch}'

# Deployment
kube_context='${kube_context}'

# Deployment storage.
storage_class='${storage_class}'
storage_access_mode='${storage_access_mode}'
database_storage_size='${database_storage_size}'
files_storage_size='${files_storage_size}'
private_files_storage_size='${private_files_storage_size}'
code_storage_size='${code_storage_size}'

# Deployment naming.
node_name='${node_name}'
namespace='${namespace}'
pod_prefix='${pod_prefix}'
source_prefix='${source_prefix}'

# Deployment secrets.
drupal_admin_password='${drupal_admin_password}'
drupal_cron_key='${drupal_cron_key}'
drupal_hash_salt='${drupal_hash_salt}'
mariadb_password='${mariadb_password}'

# Basic authentication.
basic_authentication_user='${basic_authentication_user}'
basic_authentication_password='${basic_authentication_password}'

# Mailhog.
mailhog_user='${mailhog_user}'
mailhog_password='${mailhog_password}'

# Lando secrets.
lando_drupal_admin_password='${lando_drupal_admin_password}'
lando_drupal_cron_key='${lando_drupal_cron_key}'
lando_drupal_hash_salt='${lando_drupal_hash_salt}'
lando_mariadb_password='${lando_mariadb_password}'

# User info.
user='${user}'
user_id='${user_id}'
group_id='${group_id}'

# Ssh key.
ssh_private_key='${ssh_private_key}'
EOF
  ok

  # Initialize lando and helm configuration files.
  echo "Initializing configuration files."
  cp "/app/.lando.yml.orig" "/app/.lando.yml"
  cp "${helm_dir}/values.yaml.orig" "${helm_dir}/values.yaml"
  for var in "${!vars[@]}"; do
    sed -i "s~!${var}!~${!var}~g" /app/.lando.yml
    if [ ! -z "${vars[$var]}" ]; then
      sed -i "s~!${var}!~${!var}~g" "${helm_dir}/values.yaml"
    fi
  done
  ok

  # Create crontab configmap.
  kubectl --kubeconfig "${kube_config_file}" --context "${kube_context}" create configmap -n "${namespace}" "{{ .Values.podPrefix }}-crontab" --dry-run=client --from-file=crontab=/app/crontab -o yaml > "${helm_dir}/templates/crontab-configmap.yaml"

  # Create basic authentication secret.
  if [ ! -z "${basic_authentication_user}" ] && [ ! -z "${basic_authentication_password}" ]; then
    cat <<EOF > "${helm_dir}/templates/basic-authentication-secret.yaml"
apiVersion: v1
kind: Secret
type: Opaque
metadata:
  name: "{{ .Values.podPrefix }}-basic-authentication"
  namespace: "{{ .Values.namespace }}"
data:
  auth: "$(htpasswd -nb ${basic_authentication_user} ${basic_authentication_password} | base64)"
EOF
    ok
  fi

  # Set private key.
  if [[ "${git_url}" == *git@* ]]; then
    echo "Setting private ssh key."
    echo "${ssh_private_key}" > "${ssh_private_key_file}"
    sed -i 's/^/  /g' "${ssh_private_key_file}"
    sed -i "/sshPrivateKey: |/r ${ssh_private_key_file}" "${helm_dir}/values.yaml"
    sed -i 's/^/        /g' "${ssh_private_key_file}"
    sed -i "/SSH_PRIVATE_KEY: |/r ${ssh_private_key_file}" "/app/.lando.yml"
    rm -f "/tmp/private_key"
    ok
  fi
}

function get_context() {
  # Get context.
  readarray -t kube_contexts < <(kubectl config get-contexts | grep -v 'AUTHINFO' | sed 's/ \+/ /g' | cut -d ' ' -f2)
  if [ "${#kube_contexts[@]}" -eq "0" ]; then
    # No context found.
    error "No kubernetes context found."
  elif [ "${#kube_contexts[@]}" -eq "1" ]; then
    # One context found, ask for confirmation.
    echo "Kubernetes context to use: ${kube_contexts[0]}"
    echo
    read -p "Press [enter] to confirm, any other key to exit." -n 1 -r
    if [[ $REPLY == "" ]]; then
      context="${kube_contexts[0]}"
    else
      exit
    fi
  else
    quitindex="${#kube_contexts[@]}"
    kube_contexts_arr=("${kube_contexts[@]}")
    kube_contexts_arr+=("quit")
    for index in "${!kube_contexts_arr[@]}"; do
      echo "${index}) ${kube_contexts_arr[$index]}"
    done
    answered='false'
    while [ $answered == 'false' ]; do
      read -p '' -rs -n1
      if [ "${REPLY}" == "${quitindex}" ]; then
        exit
      fi
      if [ ! -z "${kube_contexts_arr[$REPLY]}" ]; then
        answered='true'
        context="${kube_contexts_arr[$REPLY]}"
      fi
    done
  fi
}
