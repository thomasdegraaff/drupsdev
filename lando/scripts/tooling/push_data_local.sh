#!/bin/bash
source /app/lando/scripts/tooling/tooling_includes.sh

if [ ! "${LANDO_SERVICE_NAME}" = "builder" ]; then
  # Check for uncommitted code.
  check_uncommitted
fi

# Check for composer.json.
check_composer_json

if [ -f "${store_dir}/files-${git_hash}.tar.gz" ] && [ -f "${store_dir}/private-files-${git_hash}.tar.gz" ] && [ -f "${store_dir}/database-${git_hash}.gz" ]; then
  # Site storage found.

  # Drop drupal database if exists.
  result=$(mysql -u drupal --password=drupal -h mariadb -e "SHOW DATABASES" | grep "drupal")
  if [ "${result}" == "drupal" ]; then
    echo "Dropping existing database."
    mysql -u drupal --password=drupal -h mariadb -e "DROP DATABASE drupal"
  fi

  echo "Importing database from storage."
  mysql -u drupal --password=drupal -h mariadb -e "CREATE DATABASE drupal"
  zcat "${store_dir}/database-${git_hash}.gz" | mysql -u drupal --password=drupal -h mariadb drupal
  # Check if database was imported succesfully.
  error=false
  result=$(mysql -u drupal --password=drupal -h mariadb -e "SHOW DATABASES" | grep "drupal")
  if [ "${result}" == "drupal" ]; then
    echo "Database was restored succesfully from storage."
  else
    echo "Something went wrong restoring the database from storage."
    error=true
  fi
  # Remove existing files.
  echo "Removing existing public files."
  rm -rf "${site_dir}/files"
  echo "Restoring public files from storage."
  tar -xzf "${store_dir}/files-${git_hash}.tar.gz" -C "${site_dir}"
  # Check if public files were restored succesfully.
  if [ "$(ls -A ${site_dir}/files)" ]; then
    echo "Public files were restored succesfully from storage."
  else
    echo "Something went wrong restoring public files from storage."
    error=true
  fi
  # Remove existing private files.
  echo "Removing existing private files."
  rm -rf "/app/drupal/private_files"
  echo "Restoring private files from storage."
  tar -xzf "${store_dir}/private-files-${git_hash}.tar.gz" -C "/app/drupal"
  # Check if private files were restored succesfully.
  if [ "$(ls -A /app/drupal/private_files)" ]; then
    echo "Private files were restored succesfully from storage."
  else
    echo "Something went wrong restoring private files from storage."
    error=true
  fi
  if [ "${error}" = false ]; then
    # Get site out of mainenance mode.
    echo "Getting site out of maintenance mode."
    /app/vendor/bin/drush state:set system.maintenance_mode 0 --input-format=integer
    check_result $?

    echo "Finished restoring site data."

    # Import site configuration.
    source /app/drups/scripts/drupal_install.sh
    drupal_install
  else
    error "Something went wrong restoring site data."
  fi
else
  # No site storage found.
  error "No site data storage corresponding with git commit ${git_hash} was found."
fi
