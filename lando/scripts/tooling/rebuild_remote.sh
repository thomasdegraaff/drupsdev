#!/bin/bash
source /app/lando/scripts/tooling/tooling_includes.sh

# Get remote data.
/app/lando/scripts/tooling/pull_data_remote.sh

# Set user.
user="$(whoami)"
user_id="$(id -u ${user})"
group_id="$(id -g ${user})"

# Set ssh private key file location.
ssh_private_key_file=/tmp/private_key

# Define secret types.
secret_types=(
  "drupal_admin_password"
  "drupal_hash_salt"
  "drupal_cron_key"
)

# Check uncommitted changes.
check_uncommitted

# Check if kubeconfig exists.
check_cluster_accessible "${kube_context}"

# Check if deployment is ok.
check_deployment

echo "----------------------------------------------------"
echo
echo "Answer some questions to re-initialize drups."
echo

# Get deployment domain.
echo
correct=false
while [ "$correct" == 'false' ]; do
  read -e -p "Deployment domain: " -i "${domain}" domain
  if [[ $domain =~ ^([a-zA-Z0-9](([a-zA-Z0-9-]){0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,}$ ]]; then
    correct=true
  else
    echo
    echo "Set a correct domain, something like 'drups.eu'."
    echo
  fi
done

# Get deployment subdomain.
correct=false
while [ "$correct" == 'false' ]; do
  read -e -p "Deployment subdomain (may be empty): " -i "${subdomain}" subdomain
  if [[ $subdomain =~ ^([a-zA-Z0-9](([a-zA-Z0-9-]){0,61}[a-zA-Z0-9])?\.?)*$ ]]; then
    correct=true
  else
    echo
    echo "Set a correct subdomain, something like 'drups.eu'."
    echo
  fi
done

# Set site url and lando default subdomain.
if [ -z "${subdomain}" ]; then
  site_url="${domain}"
  default_lando_subdomain="${domain%.*}"
else
  site_url="${subdomain}.${domain}"
  default_lando_subdomain="${subdomain}"
fi

# Get lando subdomain.
echo
read -e -p "Lando subdomain: " -i "${lando_subdomain}" lando_subdomain
lando_url="${lando_subdomain}.lndo.site"

# Get passwords from user.
echo
echo "Set secrets for Kubernetes deployment."
for secret_type in ${secret_types[@]}; do
  if [ -z "${!secret_type}" ]; then
    default_value="$(pwgen -n 16)"
  else
    default_value="${!secret_type}"
  fi
  label="$(echo "${secret_type^}" | sed 's/_/ /g')"
  read -e -p "${label}: " -i "${default_value}" "${secret_type}"
done

# Get basic authentication.
echo
echo "Basic authentication (leave empty to disable basic authentication)"
read -e -p "user: " -i "${basic_authentication_user}" basic_authentication_user
read -e -p "password: " -i "${basic_authentication_password}" basic_authentication_password
echo
echo "Mailhog authentication (leave empty to disable authentication)"
read -e -p "user: " -i "${mailhog_user}" mailhog_user
read -e -p "password: " -i "${mailhog_password}" mailhog_password

# Get new images.
echo "Getting new drups-images file from drups-lando repository."
wget -q -O /app/drups-images https://gitlab.com/thomasdegraaff/drups/-/raw/main/drups-images
if [ $? -eq 0 ]; then
  echo "Latest drups-images specs file downloaded."
  source /app/drups-images
else
  echo "Could not retrieve latest drups-images specs file from drups repository, using current drups image specs as defaults."
fi
images=(
  "php_image"
  "lando_php_image"
  "drupal_tools_image"
  "lando_tools_image"
  "nginx_image"
  "varnish_image"
  "redis_image"
  "mailhog_image"
  "mariadb_image"
)
echo "Docker images to use:"
for image in ${images[@]}; do
  label="$(echo "${image^}" | sed 's/_/ /g')"
  read -e -p "${label}: " -i "${!image}" "${image}"
done

# Get storage sizes.
echo
echo "What storage sizes do you want?"
read -e -p "Code storage size: " -i "${code_storage_size}" code_storage_size
read -e -p "Database storage size: " -i "${database_storage_size}" database_storage_size
read -e -p "Files storage size: " -i "${files_storage_size}" files_storage_size
read -e -p "Private files storage size: " -i "${private_files_storage_size}" private_files_storage_size

# Show values for confirmation.
cat <<EOF



Check if the following is ok.

General
-------
project repo: ${git_url}
branch: ${git_branch}

Remote deployment
-----------------
public site url: https://${site_url}
namespace: ${namespace}
pod prefix: ${pod_prefix}

drupal:
admin password: ${drupal_admin_password}
cron key: ${drupal_cron_key}
hash salt: ${drupal_hash_salt}

basic authentication:
user: ${basic_authentication_user}
password: ${basic_authentication_password}

mailhog:
user: ${mailhog_user}
password: ${mailhog_password}

code storage size: ${code_storage_size}
database storage size: ${database_storage_size}
files storage size: ${files_storage_size}
private files storage size: ${private_files_storage_size}

Lando
-----
local url: https://${lando_url}

secrets:
drupal admin password: ${lando_drupal_admin_password}
drupal hash salt: ${lando_drupal_hash_salt}
drupal cron key: ${lando_drupal_cron_key}
mariadb password: ${lando_mariadb_password}

Docker images
-------------
EOF

for image in ${images[@]}; do
  label="$(echo "${image^}" | sed 's/_/ /g')"
  echo "${label}: ${!image}"
done

echo
read -p "Press [enter] to confirm, any other key to quit." -n 1 -r
echo
if [[ $REPLY != "" ]]; then
  exit 0
fi

# Store drups config and create lando and helm config files.
create_config

# Store Docker images.
echo '# Drups docker images.' > /app/drups-images
for image in ${images[@]}; do
  echo "${image}='${!image}'" >> /app/drups-images
done

# Set deployment in maintenance mode.
set_maintenance_mode "${pod_prefix}" "deployment" 1 "${kube_context}"

# Upgrade deployment.
echo "Upgrading ${git_branch} branch deployment in Kubernetes."
helm --kubeconfig  "${kube_config_file}" --kube-context "${kube_context}" upgrade --wait --create-namespace -n "${namespace}" "${namespace}-${pod_prefix}" /app/kube/helm/drups
check_result $?

# Wait until site deployment is ready.
wait_for_drupal_pod

# Get deployment pod names.
echo "Getting deployment pod names."
echo 'drupal-tools'
deploy_pod_name="$(get_pod_name "${pod_prefix}" 'drupal-tools' "${kube_context}")"
check_result $?
echo 'mariadb'
deploy_mariadb_pod_name="$(get_pod_name "${pod_prefix}" 'mariadb' "${kube_context}")"
check_result $?

# Wait until database ready.
echo "Waiting until database is ready."
execute_job "${deploy_pod_name}" 'drupal-tools' '/app/drups/scripts/check_database.sh' "${kube_context}"
check_result $?

# Install or update Drupal.
echo "Installing or updating Drupal."
execute_job "${deploy_pod_name}" 'drupal-tools' '/app/kube/scripts/drupal_install.sh' "${kube_context}"
check_result $?

# Clear varnish cache.
varnish_cache_clear "${git_branch}" "${pod_prefix}" "${kube_context}"

cat <<EOF
Drups is re-initialized.

Remote deployment
-----------------
url: https://${site_url}/user/login

EOF

if [ ! -z "${basic_authentication_user}" ] && [ ! -z "${basic_authentication_password}" ]; then
cat <<EOF
basic authentication:
user: ${basic_authentication_user}
password: ${basic_authentication_password}

EOF
fi

cat <<EOF
drupal:
user: admin
password: ${drupal_admin_password}

mailhog:
url: https://mailhog.${site_url}
user: ${mailhog_user}
password: ${mailhog_password}

Local deployment (not running yet)
----------------------------------
url: https://${lando_url}/user/login

drupal:
user: admin
password: drupal

mailhog:
url: https://mailhog.${lando_url}

Now execute 'lando start' to startup the local deployment.
EOF
