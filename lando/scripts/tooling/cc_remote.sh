#!/bin/bash
source /app/lando/scripts/tooling/tooling_includes.sh

echo "Clearing opcache for ${git_branch} branch deployment."
pod_name="$(get_pod_name "${pod_prefix}" 'php' "${kube_context}")"
execute_job "${pod_name}" 'php' 'kill -USR2 1' "${kube_context}"

echo "Rebuilding Drupal cache for ${git_branch} branch deployment."
pod_name="$(get_pod_name "${pod_prefix}" 'drupal-tools' "${kube_context}")"
execute_job "${pod_name}" 'drupal-tools' '/app/vendor/bin/drush cache:rebuild' "${kube_context}"

varnish_cache_clear "${git_branch}" "${pod_prefix}" "${kube_context}"
