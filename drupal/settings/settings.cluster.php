<?php

// Miscancelaneous settings.
$settings['trusted_host_patterns'] = ['^.+$'];

// Override local.settings.php settings.
$settings['rebuild_access'] = FALSE;
$config['system.performance']['css']['preprocess'] = TRUE;
$config['system.performance']['js']['preprocess'] = TRUE;
$settings['skip_permissions_hardening'] = FALSE;
